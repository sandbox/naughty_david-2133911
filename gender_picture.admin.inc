<?php
/**
 * Gender Picture admin pages
 */

/**
 * Admin form to set picture for gender
 */
function gender_picture_admin(){

	$form['gender_picture_male'] = array(
		'#type' => 'managed_file',		
		'#title' => t('Upload Male picture'),
		'#default_value' => variable_get('gender_picture_male', ''),
		'#description' => t("Upload Male users default picture"),
		'#required' => TRUE,
		'#upload_location' => 'public://default_user_pic/'
	);

	$form['gender_picture_female'] = array(
		'#type' => 'managed_file',
		'#title' => t('Upload Female Picture'),
		'#default_value' => variable_get('gender_picture_female', ''),
		'#description' => t("Upload Female users default picture"),
		'#required' => TRUE,
		'#upload_location' => 'public://default_user_pic/'
	);

	$form['#attributes'] = array('enctype' => "multipart/form-data");

	return system_settings_form($form);
}