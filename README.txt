Gender Picture module Provides seperate default user pictures
for users based on their Gender. This module creates a profile field 
named gender. And its with respect to this that it assign pictures to 
users.

Installation
============
The modules does't have any dependencies. You can directly install it.
I needs user pictures to be enabled.
After installation you can set the default picture for users from the
administration page.